#include "pwm_sin.h"

void init_tables_Sin(void)
{
	// size_Sin	(1/4) = 10
	Sin10[0]		=  38;
	Sin10[1]		=  74;
	Sin10[2]		= 109;
	Sin10[3]		= 141;
	Sin10[4]		= 170;
	Sin10[5]		= 194;
	Sin10[6]		= 214;
	Sin10[7]		= 228;
	Sin10[8]		= 237;
	Sin10[9]		= 240;
	
	// size_Sin	(1/4) = 20
	Sin20[0]		=   0;
	Sin20[1]		=  10;
	Sin20[2]		=  20;
	Sin20[3]		=  29;
	Sin20[4]		=  39;
	Sin20[5]		=  48;
	Sin20[6]		=  57;
	Sin20[7]		=  65;
	Sin20[8]		=  73;
	Sin20[9]		=  81;
	Sin20[10]		=  88;
	Sin20[11]		=  95;
	Sin20[12]		= 101;
	Sin20[13]		= 107;
	Sin20[14]		= 111;
	Sin20[15]		= 115;
	Sin20[16]		= 119;
	Sin20[17]		= 122;
	Sin20[18]		= 123;
	Sin20[19]		= 125;

	// size_Sin	(1/4) = 50
	Sin50[0]   =   0;
	Sin50[1]   =  12;
	Sin50[2]   =  24;
	Sin50[3]   =  35;
	Sin50[4]   =  47;
	Sin50[5]   =  59;
	Sin50[6]   =  70;
	Sin50[7]   =  82;
	Sin50[8]   =  93;
	Sin50[9]   = 105;
	Sin50[10]  = 116;
	Sin50[11]  = 127;
	Sin50[12]  = 138;
	Sin50[13]  = 149;
	Sin50[14]  = 160;
	Sin50[15]  = 170;
	Sin50[16]  = 181;
	Sin50[17]  = 191;
	Sin50[18]  = 201;
	Sin50[19]  = 211;
	Sin50[20]  = 220;
	Sin50[21]  = 230;
	Sin50[22]  = 239;
	Sin50[23]  = 248;
	Sin50[24]  = 257;
	Sin50[25]  = 265;
	Sin50[26]  = 273;
	Sin50[27]  = 281;
	Sin50[28]  = 289;
	Sin50[29]  = 296;
	Sin50[30]  = 303;
	Sin50[31]  = 310;
	Sin50[32]  = 317;
	Sin50[33]  = 323;
	Sin50[34]  = 329;
	Sin50[35]  = 334;
	Sin50[36]  = 339;
	Sin50[37]  = 344;
	Sin50[38]  = 349;
	Sin50[39]  = 353;
	Sin50[40]  = 357;
	Sin50[41]  = 360;
	Sin50[42]  = 363;
	Sin50[43]  = 366;
	Sin50[44]  = 368;
	Sin50[45]  = 370;
	Sin50[46]  = 372;
	Sin50[47]  = 373;
	Sin50[48]  = 374;
	Sin50[49]  = 375;
}

void set_parameters_sin(void)
{
	U0				= pSin[size_Sin - 1];

	max_step_sin	= size_Sin * 4;
	step_Sin_1_4	= max_step_sin / 4;
	step_Sin_2_4	= max_step_sin / 2;
	step_Sin_3_4	= (max_step_sin * 3) / 4;
}