

#ifndef SPI_H_
#define SPI_H_

#include "main.h"

void SPI_Init(void);
void transfer_Data_SPI(char);
void generate_Writing_Pulse(void);

#endif /* SPI_H_ */