
#ifndef LED_H_
#define LED_H_

//Define Digits 7-seg. Indicators
#define digit0	0b00111111
#define digit1	0b00000110
#define digit2	0b01011011
#define digit3	0b01001111
#define digit4	0b01100110
#define digit5	0b01101101
#define digit6	0b01111101
#define digit7	0b00000111
#define digit8	0b01111111
#define digit9	0b01101111
#define dot		0b10000000
#define empty	0

char			Leds[4];

void			storeLEDs(char, char);
unsigned int	power10(char);

#endif /* LED_H_ */