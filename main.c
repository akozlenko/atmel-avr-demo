#include "main.h"

void port_ini(void);

int main(void)
{
	port_ini();
	ADC_Init();
	Timer0_Init();
	SPI_Init();
	
	init_tables_Sin();	
	Timer1_Init(Get_variant_Sin());		// Call after init Sin !
	
	sei();								//Enable Global Interrupt
	
	while (1)
	{

	}
}

void port_ini(void)
{
	DDRB	|= (1<<PB1) | (1<<PB2)|(1<<PB3)|(1<<PB5);	// PB1, SPI - out
	PORTB	&= ~((1<<PB2)|(1<<PB3)|(1<<PB5));			// low in SPI pins
	PORTB	|= (1<< PB0);								// Hi in Pin B0 (On Pull-up resistor)
	
	// PORTC - Input
	PORTC	|= (1 << PC1) | (1 << PC2);					// Hi in Pin C1, C2 (On Pull-up resistor)
	
}

void GetPinTest(void)
{
	if (Port_Test & (1 << Pin_Test))
		Port_Test &= ~(1 << Pin_Test);	// set 0 if  1
	else
		Port_Test |= (1 << Pin_Test);	// set 1 if  0
}

unsigned char Get_variant_Sin(void)
{
 	return 0b00000011 & (~(PINC >> 1));
}