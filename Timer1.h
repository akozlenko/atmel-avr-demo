#ifndef TIMER1_H_
#define TIMER1_H_

#include "main.h"

unsigned char		size_Sin;
unsigned int		Sin10[10];
unsigned int		Sin20[20];
unsigned int		Sin50[50];

unsigned int*		pSin;

unsigned int		U0;

unsigned char		max_step_sin;
unsigned char		step_Sin_1_4;
unsigned char		step_Sin_2_4;
unsigned char		step_Sin_3_4;

unsigned char		synchro;

void Timer1_Init(unsigned char);

#endif /* TIMER1_H_ */