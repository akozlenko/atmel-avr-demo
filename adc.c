#include "adc.h"

void ADC_Init(void)
{
	ADCSRA	= (1 << ADEN)												// ADC Enabled
			|(1<<ADSC)													// Launch ADC
			| (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0)				// Divider = 128 (8 MGz / 128 = 62.5 kGz)
			|(1<<ADFR)													// Continuous operation of ADC
			|(1<<ADIE)													// Enable ADC Interrupt
	;
	ADMUX	= (1 << REFS1) | (1 << REFS0)								// Vref = 2.56 V (internal)
			| (0 << MUX3) | (0 << MUX2) | (0 << MUX1) | (0 << MUX0)		// Input = ADC0 (PC0)
	;
}


ISR(ADC_vect)
{
	char high_adc = 0, low_adc = 0;
	
	low_adc		= ADCL;		// ADCL must be read FIRST !!!
	high_adc	= ADCH;
	
	if (isSimulator)
	{
		high_adc = 0x04;
		low_adc	 = 0xD2;	// test = 1234
	};
	
	storeLEDs(high_adc, low_adc);
}

