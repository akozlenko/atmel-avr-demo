#include "Timer1.h"

static unsigned char step_Sin = 1;

void Timer1_Init(unsigned char variant)
{
 	switch (variant) 
	{
		case 1:		// 9-bit PWM on 2 kGZ (40 samples per period)
		{	
 			TCCR1A	|= (1 << COM1A1)				// Clear OC1A on Compare Match, set at BOTTOM, (non-inverting mode) 					|  (1 << WGM11);				// Fast PWM, Overflow interrupt in TOP,
			TCCR1B	|= (1 << WGM13) | (1 << WGM12);	// update OCR1A in TOP = ICR = 500 = 0x01F4
 			ICR1H	= 0x01;							// (1 mks * 500 = 500 mks => 2 kGz)
 			ICR1L	= 0xF4;							// (50 Gz => 20 ms / 500 mks = 40 samples per period = 10 samples in 1/4 period)
 			
 			TCCR1B	|= (1 << CS11); 				// Divider = 1:8;  (1 MGz => 1 mks)
			TIMSK	|= (1 << TOIE1);				// Interrupt Overflow Enable
			
			size_Sin	= 10;
			pSin		= &Sin10[0];
			
			break;
		}
		case 2:		// 8-bit PWM on 4 kGZ (80 samples per period)
		{	
			TCCR1A	|= (1 << COM1A1)				// Clear OC1A on Compare Match, set at BOTTOM, (non-inverting mode)					|  (1 << WGM11);				// Fast PWM, Overflow interrupt in TOP,
			TCCR1B	|= (1 << WGM13) | (1 << WGM12);	// update OCR1A in TOP = ICR = 250 = 0x00FA
			ICR1H	= 0x00;							// (1 mks * 250 = 250 mks => 4 kGz)
			ICR1L	= 0xFA;							// (50 Gz => 20 ms / 250 mks = 80 samples per period = 20 samples in 1/4 period)
 			
			TCCR1B	|= (1 << CS11); 				// Divider = 1:8;  (8 MGz : 8 = 1 MGz => 1 mks) 
			TIMSK	|= (1 << TOIE1);				// Interrupt Overflow Enable

			size_Sin	= 20;
			pSin		= &Sin20[0];	
			
			break;
		}
		case 3:		// 10-bit PWM on 10 kGZ (200 samples per period)
		{	
 			TCCR1A	|= (1 << COM1A1)				// Clear OC1A on Compare Match, set at BOTTOM, (non-inverting mode) 					|  (1 << WGM11);				// Fast PWM, Overflow interrupt in TOP,
 			TCCR1B	|= (1 << WGM13) | (1 << WGM12);	// update OCR1A in TOP = ICR = 800 = 0x0320
 			ICR1H	= 0x03;							// (0.125 mks * 800 = 100 mks => 10 kGz)
 			ICR1L	= 0x20;							// (50 Gz => 20 ms / 100 mks = 200 samples per period = 50 samples in 1/4 period)
 			
			TCCR1B	|= (1 << CS10); 				// Divider = 1:1;  (8 MGz => 0.125 mks)
			TIMSK	|= (1 << TOIE1);				// Interrupt Overflow Enable

			size_Sin	= 50;
			pSin		= &Sin50[0];
			
			break;
		}
		default:		// NO PWM
		{
			TCCR1B		= 0x00;							// Timer1 stopped
			TCCR1A		= 0x00;							
 			TIMSK		&= ~(1 << TOIE1);				// Interrupt Overflow disable
			
			pSin		= NULL;	
			size_Sin	= 1;
		} 
	}		
	
	if (pSin != NULL) 	set_parameters_sin();
}

ISR(TIMER1_OVF_vect)
{
	unsigned char index	= 0;

	step_Sin++;
	if ((step_Sin > max_step_sin) || (step_Sin <= 0))	step_Sin = 1;
	
	if (step_Sin > step_Sin_3_4)
	{	// 4-th quarter Sin
		index		= size_Sin - (step_Sin - step_Sin_3_4) - 1;
		OCR1A = U0 - pSin[index];
	}
	else if (step_Sin > step_Sin_2_4)
	{	// 3-rd quarter Sin
		index		= step_Sin - step_Sin_2_4 - 1;
		OCR1A = U0 - pSin[index];
	}
	else if (step_Sin > step_Sin_1_4)
	{	// 2-nd quarter Sin
		index		= size_Sin - (step_Sin - step_Sin_1_4) - 1;
		OCR1A = U0 + pSin[index];
	}
	else
	{	// 1-st quarter Sin
		index		= step_Sin - 1;
		OCR1A = U0 + pSin[index];
	}
} //ISR(TIMER1_OVF_vect)



