
#ifndef MAIN_H_
#define MAIN_H_

#define isSimulator 1	//// = 1 ONLY SIMULATOR

#define F_CPU 8000000UL

#include <avr/io.h>
//#include <avr/iom8.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#include "adc.h"
#include "Timer0.h"
#include "Timer1.h"
#include "SPI.h"
#include "led.h"
#include "pwm_sin.h"

#define Port_Test	PORTC
#define Pin_Test	PC1

unsigned char Get_variant_Sin(void);
void		  GetPinTest(void);

#endif /* MAIN_H_ */