
#ifndef PWM_SIN_H_
#define PWM_SIN_H_

#include "main.h"

void init_tables_Sin(void);
void set_parameters_sin(void);

#endif /* PWM_SIN_H_ */