#include "SPI.h"

void SPI_Init(void)
{
	SPCR	= ((1<<SPE)|(1<<MSTR));				// SPI enabled, Mode Master
}

void transfer_Data_SPI(char Data)
{
	SPDR	= Data;					
	while(!(SPSR & (1<<SPIF)));
}

void generate_Writing_Pulse(void)
{							//   _____
	PORTB |= (1<<PB2);		//   |   |           __ 
	PORTB &= ~(1<<PB2);		// __|   |__    PB2 (SS)
}