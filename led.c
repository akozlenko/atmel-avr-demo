#include "led.h"
#include "main.h"

char Digits[] = {digit0, digit1, digit2, digit3, digit4, digit5, digit6, digit7, digit8, digit9, dot, empty};

void storeLEDs(char highReg, char lowReg)
{
	unsigned int	Temp, multiplicator;
	unsigned char	i, index;
	unsigned int	inputValue = highReg;
	
	inputValue = (inputValue << 8) + lowReg;
	
	cli();	//disable Global Interrupt
	for (i = 0; i < 4; i++)
	{
		index			= 3-i;
		multiplicator	= power10(index);
		
		Temp		=  inputValue / multiplicator;
		
		if (PINB & (1 << PINB0))			//Button Off
		{
			Leds[index]	=  Digits[Temp];
		}
		else								//Button On
		{
			Leds[3-index]	=  Digits[Temp];
		};
		
		inputValue  -= Temp * multiplicator;
	}
	sei();	//Enable Global Interrupt
}


unsigned int power10(char n)
{
	if(n == 0) return 1;
	return 10 * power10(n - 1);
}

