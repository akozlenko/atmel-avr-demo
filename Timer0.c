#include "Timer0.h"

static unsigned char	indexInd = 0;

void Timer0_Init(void)
{
	TCCR0 |= (1 << CS02) ;		// Divider = 1:256;  (31.250 kGz => 32 mks)
	TIMSK |= (1 << TOIE0);		// Interrupt Overflow enable (32 mks * 256 = 8.2 ms => x 4 LED = 32.8 ms = 30 Gz)
}

ISR(TIMER0_OVF_vect)
{
	GetPinTest();

	//Off LEDs
 	transfer_Data_SPI(0xff);
 	transfer_Data_SPI(0);
 	generate_Writing_Pulse();

 	if (indexInd > 3) indexInd = 0;

 	transfer_Data_SPI(~(1 << indexInd));	// On i-LED
 	transfer_Data_SPI(Leds[indexInd]);		// Load Data to LEDs
 	generate_Writing_Pulse();

 	indexInd++;
}